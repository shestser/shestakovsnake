-Hra Snake:
Toto je jednoduchá hra Snake implementovaná v jazyce C++ s využitím knihovny ncurses pro práci s terminálem.
Hra běží v konzoli a hráč ovládá hada pomocí kláves "a", "d", "w", "s".

-Požadavky:
C++ kompilátor
Knihovna ncurses

-Implementace:
Hra je implementována pomocí tří funkcí spuštěných  samostatných vláknech:

Draw: Vykresluje herní pole a aktuální stav hry pomocí knihovny ncurses.
Input: Čte vstup od hráče a nastavuje směr hada podle stisknuté klávesy.
Logic: Zajišťuje logiku hry - pohyb hada, detekce kolizí a aktualizace skóre.
Při inicializaci hry jsou nastaveny počáteční hodnoty, včetně umístění hada a ovoce. Hra běží 
ve smyčce, dokud hráč neukončí hru stiskem klávesy "q" nebo nedojde k prohře.

-Funkcionalita a ovládání:
Ovládání hada: "a" (doleva), "d" (doprava), "w" (nahoru), "s" (dolů)
Ukončení hry: "q"
Cílem hry je sníst co nejvíce ovoce a dosáhnout co nejvyššího skóre. Při každém snězení ovoce se 
had prodlouží o jedno článek. Hra končí, když had narazí do stěny nebo do svého těla.

-Výsledky běhu programu a naměřené časy:
 Výsledky běhu programu mohou být závislé na hardwaru a prostředí, ve kterém je hra spuštěna. Naměřené
 časy pro jedno- a vícevláknovou verzi nejsou v kódu explicitně uvedeny. Je doporučeno provést měření a 
 porovnání výkonu s ohledem na specifické podmínky běhu programu.

-Spouštění hry:
# Přepnutí do adresáře build
cd build
# Překlad pomocí CMake
cmake ..
# Překlad projektu
cmake --build .
# Spuštění hry
./ShestakovSnake

-Závěr
Tato jednoduchá hra Snake je navržena tak, aby demonstrovala práci s vlákny a základy práce v terminálu 
pomocí knihovny ncurses. Můžete si s kódem hrát, přidávat vlastní funkce a vylepšení nebo experimentovat s optimalizacemi