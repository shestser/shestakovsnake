
#pragma once

 #include <iostream>
#include <ncurses.h>
#include <thread>
#include <chrono>
#include <mutex>


using namespace std;

mutex mtx; // Mutex pro zajištění bezpečnosti při přístupu více vlákny
bool gameOver; // Příznak ukončení hry
const int width = 40; // Šířka herního pole
const int height = 20; // Výška herního pole
int x, y, fruitX, fruitY, score; // Pozice hráče a ovoce, aktuální skóre
int tailX[100] = {0}, tailY[100] = {0}; // Pozice segmentů ocasu hada
int nTail; // Délka ocasu

enum eDirection { STOP = 0, LEFT, RIGHT, UP, DOWN }; // Výčet pro určení směru pohybu hada
eDirection dir; // Aktuální směr hada

/*
 * Nastaví počáteční hodnoty pro hru, včetně umístění hada a ovoce.
 * Nastaví herní proměnné, jako je gameOver, směr hada, skóre, atd.
 */

void Setup();

/*
 * Vykreslí herní pole a aktuální stav hry.
 * Používá knihovnu ncurses pro kreslení na terminálu.
 */
void Draw();

/*
 * Čte vstup od hráče a nastavuje směr hada podle stisknuté klávesy.
 * Umožňuje hráči ovládat hada pomocí kláves "a", "d", "w", "s" a ukončit hru klávesou "q".
 */

void Input();

/*
 * Vypíše zprávu o prohře a zobrazí skóre.
 * Počká několik sekund před ukončením hry.
 */

void Lost();

/*
 * Logika hry - pohyb hada, detekce kolizí a aktualizace skóre.
 * Hra běží ve smyčce, dokud není gameOver nastaveno na true.
 */

void Logic();

