#include "ShestakovSnake.hpp"

void Setup() {
    gameOver = false;
    dir = STOP;
    x = width / 2 - 1;
    y = height / 2 - 1;
    fruitX = rand() % width;
    fruitY = rand() % height;
    score = 0;
    nTail = 0;
}

void Draw() {
    while (!gameOver) {
        {
            lock_guard<mutex> lock(mtx);
            clear();

            // Vykreslení horního okraje
            for (int i = 0; i < width + 2; i++)
                mvprintw(0, i, "#");

            // Vykreslení hrací plochy
            for (int i = 0; i < height; i++) {
                mvprintw(i + 1, 0, "#");  // Levý okraj
                for (int j = 0; j < width; j++) {
                    if (i == y && j == x)
                        mvprintw(i + 1, j + 1, "O");
                    else if (i == fruitY && j == fruitX)
                        mvprintw(i + 1, j + 1, "@");
                    else {
                        bool print = false;
                        for (int k = 0; k < nTail; k++) {
                            if (tailX[k] == j && tailY[k] == i) {
                                print = true;
                                mvprintw(i + 1, j + 1, "o");
                            }
                        }
                        if (!print) {
                            mvprintw(i + 1, j + 1, " ");
                        }
                    }
                }
                mvprintw(i + 1, width + 1, "#");  // Pravý okraj
            }

            // Vykreslení spodního okraje
            for (int i = 0; i < width + 2; i++)
                mvprintw(height + 1, i, "#");

            mvprintw(height + 2, 0, "Score: %d", score);

            refresh();
        }

        this_thread::sleep_for(chrono::milliseconds(700));
        std::this_thread::yield();
    }
}

void Input() {
    while (!gameOver) {
        int ch = getch();
        {
            lock_guard<mutex> lock(mtx);
            switch (ch) {
                case 'a':
                    dir = LEFT;
                    break;
                case 'd':
                    dir = RIGHT;
                    break;
                case 'w':
                    dir = UP;
                    break;
                case 's':
                    dir = DOWN;
                    break;
                case 'q':
                    gameOver = true;
                    break;
            }
        }
        std::this_thread::yield();
    }
}

void Lost() {
    clear();
    mvprintw(1, 0, "#############################################");
    mvprintw(2, 0, "#                Prohrál jsi!               #");
    mvprintw(3, 0, "#               Tvoje skóre: %d               #", score);
    mvprintw(4, 0, "#############################################");
    refresh();  // Obnovení obrazovky po vykreslení zprávy
    std::this_thread::sleep_for(chrono::seconds(3));  // Počkej nějaký čas
}

void Logic() {
    while (!gameOver) {
        {
            lock_guard<mutex> lock(mtx);
            int prevX = tailX[0];
            int prevY = tailY[0];
            int prev2X, prev2Y;
            tailX[0] = x;
            tailY[0] = y;

            for (int i = 1; i < nTail; i++) {
                prev2X = tailX[i];
                prev2Y = tailY[i];
                tailX[i] = prevX;
                tailY[i] = prevY;
                prevX = prev2X;
                prevY = prev2Y;
            }

            switch (dir) {
                case LEFT:
                    x--;
                    break;
                case RIGHT:
                    x++;
                    break;
                case UP:
                    y--;
                    break;
                case DOWN:
                    y++;
                    break;
            }

            if (x >= width - 1 || x < 0 || y >= height || y < 0)
                gameOver = true;

            for (int i = 0; i < nTail; i++) {
                if (tailX[i] == x && tailY[i] == y)
                    gameOver = true;
            }

            if (x == fruitX && y == fruitY) {
                score += 10;
                fruitX = rand() % width;
                fruitY = rand() % height;
                nTail++;
            }
        }

        this_thread::sleep_for(chrono::milliseconds(300));
        std::this_thread::yield();
    }
}

int main() {
    initscr();  // Inicializace ncurses
    keypad(stdscr, TRUE);  // Povolení práce s klávesami
    noecho();  // Nezobrazovat zadávané znaky
    timeout(0);  // Nečekat na vstup, pokud nejsou k dispozici znaky

    Setup();

    thread t1(Draw);
    thread t2(Input);
    thread t3(Logic);

    t2.detach();
    t3.detach();

    while (!gameOver) {
        this_thread::sleep_for(chrono::milliseconds(500));
        std::this_thread::yield();
    }

    t1.join();

    Lost();

    endwin();  // Ukončení práce s ncurses

    return 0;
}
